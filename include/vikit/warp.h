#include <vikit/global.h>
#include <vikit/pinhole_camera.h>

namespace rokit {
namespace warp {

void getAffineWarpMatrix(
    const PinholeCamera* camera,
    const Vector2d& px_ref,
    const Vector3d& f_ref,
    const double depth_ref,
    const SE3& T_cur_ref,
    Matrix2d& A_cur_ref);

} // namespace warp 
} // namespace rokit
