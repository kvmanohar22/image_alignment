# vikit

This is a collation of few specific implementation for SLAM / VO / VIO. In particular the package implements the following:

- [sparse image alignment](include/vikit/image_alignment.h): Estimates 6-DoF rigid body camera motion using direct image alignment. The method is very efficient since it leverages inverse-compositional Lucas-Kanade for warping the image patches parameterized by 6-DoF rigid body transformation. This results in constant jacobian throughout all the iterations and thereby reducing the computational complexity drastically.

## Analysis
1. Estimated translation error in sparse image alignment approach as a function of current image index.

<div class="fig figcenter fighighlight">
  <img src="./imgs/alignment_error_caa477a.svg" style="margin-right:1px;">
</div>

2. Same results as above except here we compare the results against features extracted from multiple scales.
<div class="fig figcenter fighighlight">
  <img src="./imgs/image_alignment_multi_scale.svg" style="margin-right:1px;">
</div>

## Prerequisites

Install the following dependencies:
```
sudo apt-get install libeigen3-dev libopencv-dev
```

In addition, the following packages are required:

- Sophus (for representing Lie algebra elements): Available at: https://github.com/strasdat/Sophus.git
- Fast feature detector (for detecting features in the image): Available at: https://github.com/uzh-rpg/fast.git
- ceres solver (for solving non-linear optimization problem): Available at: https://github.com/ceres-solver/ceres-solver.git

## Build

```
git clone https://gitlab.com/kvmanohar22/image_alignment.git
cd image_alignment
mkdir build
cd build
cmake ..
make test_image_align
```

## Run
- Download the dataset from: http://rpg.ifi.uzh.ch/fov.html (Perspective)
- Change the values for the variables in the file: [test/test_alignment.cpp](https://gitlab.com/kvmanohar22/image_alignment/-/blob/master/test/test_alignment.cpp#L246-249)
- Run the setup as:

```
cd build
./test_image_align
```
