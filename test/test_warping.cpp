#include "vikit/utils.h"
#include "opencv2/imgproc.hpp"
#include <vikit/warp.h>
#include <vikit/utils.h>

using namespace rokit;

void drawRectangle(
    const Vector2d& px,
    const int half_patch_size,
    cv::Mat& img)
{
  cv::Scalar color(255, 0, 0);
  cv::line(img,
      cv::Point2f(px(0)-half_patch_size, px(1)-half_patch_size),
      cv::Point2f(px(0)-half_patch_size, px(1)+half_patch_size),
      color, 1, cv::LINE_AA);
  cv::line(img,
      cv::Point2f(px(0)-half_patch_size, px(1)-half_patch_size),
      cv::Point2f(px(0)+half_patch_size, px(1)-half_patch_size),
      color, 1, cv::LINE_AA);
  cv::line(img,
      cv::Point2f(px(0)+half_patch_size, px(1)+half_patch_size),
      cv::Point2f(px(0)+half_patch_size, px(1)-half_patch_size),
      color, 1, cv::LINE_AA);
  cv::line(img,
      cv::Point2f(px(0)+half_patch_size, px(1)+half_patch_size),
      cv::Point2f(px(0)-half_patch_size, px(1)+half_patch_size),
      color, 1, cv::LINE_AA);
}

int main()
{
  cv::Mat img_ref(1000, 1000, CV_8UC1, cv::Scalar(0));
  cv::Mat img_cur(1000, 1000, CV_8UC1, cv::Scalar(0));

  const int half_patch_size = 50;
  const int patch_size = half_patch_size*2;

  Vector2d px_ref(250, 250);
  drawRectangle(px_ref, half_patch_size, img_ref);
  utils::displayImg("img ref", img_ref);


}

