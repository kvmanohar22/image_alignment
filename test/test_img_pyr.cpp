#include <vikit/utils.h>

using namespace rokit;
int main()
{
  const std::string root_dir = std::getenv("VIKIT_DIR");
  string path(root_dir+"/imgs/synthetic.png");  
  cv::Mat img;
  utils::loadImg(path, img);

  ImgPyr pyr;
  utils::constructImgPyramid(img, pyr);
  std::cout << "pyramid size = " << pyr.size() << std::endl;
  int idx=1;
  for(const auto& img: pyr)
  {
    std::cout << "lvl = " << idx << "\t size = " << img.size << std::endl; 
    utils::displayImg("pyr", img);
    ++idx;
  }
}

