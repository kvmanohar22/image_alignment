#include <vikit/warp.h>

namespace rokit {
namespace warp {

void getAffineWarpMatrix(
    const PinholeCamera* camera,
    const Vector2d& px_ref,
    const Vector3d& f_ref,
    const double depth_ref,
    const SE3& T_cur_ref,
    Matrix2d& A_cur_ref)
{
  const int halfpatch_size = 5;
  const Vector3d xyz_ref(f_ref*depth_ref);
  Vector3d xyz_du_ref(camera->img2cam(px_ref + Vector2d(halfpatch_size,0)));
  Vector3d xyz_dv_ref(camera->img2cam(px_ref + Vector2d(0,halfpatch_size)));
  xyz_du_ref *= xyz_ref[2]/xyz_du_ref[2];
  xyz_dv_ref *= xyz_ref[2]/xyz_dv_ref[2];
  const Vector2d px_cur(camera->cam2img(T_cur_ref*(xyz_ref)));
  const Vector2d px_du(camera->cam2img(T_cur_ref*(xyz_du_ref)));
  const Vector2d px_dv(camera->cam2img(T_cur_ref*(xyz_dv_ref)));
  A_cur_ref.col(0) = (px_du - px_cur)/halfpatch_size;
  A_cur_ref.col(1) = (px_dv - px_cur)/halfpatch_size;
}

} // namespace warp
} // namespace rokit
